package com.dkarmazi.android.myapplication;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

public class CustomView extends FrameLayout {
    public interface CloseButtonClickListener {
        void onCloseButtonClicked(int position);
    }

    private TextView positionView;
    private TextView titleView;
    private View closeView;
    private CloseButtonClickListener closeButtonClickListener;
    private int position;

    public CustomView(Context context) {
        super(context);
        init();
    }

    public CustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.custom_view, this);
        positionView = (TextView) findViewById(R.id.custom_view_position);
        titleView = (TextView) findViewById(R.id.custom_view_title);
        closeView = findViewById(R.id.custom_view_close_button);

        closeView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(closeButtonClickListener != null) {
                    closeButtonClickListener.onCloseButtonClicked(position);
                }
            }
        });
    }

    public void setData(int position, String title, CloseButtonClickListener closeButtonClickListener) {
        this.position = position;
        this.positionView.setText("" + position);
        this.titleView.setText(title);
        this.closeButtonClickListener = closeButtonClickListener;
    }
}

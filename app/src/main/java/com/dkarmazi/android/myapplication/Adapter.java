package com.dkarmazi.android.myapplication;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class Adapter extends RecyclerView.Adapter {
    public interface ItemClickListener {
        void onItemClicked(View v, int position, String title);
    }

    private Context context;
    private ItemClickListener itemClickListener;
    private List<String> dataList;

    public Adapter(Context context, ItemClickListener itemClickListener, List<String> dataList) {
        this.context = context;
        this.itemClickListener = itemClickListener;
        this.dataList = dataList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_view_item, null, false);

        return new MyViewHolder(view, new OnRecyclerItemClickListener());
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((MyViewHolder) holder).onRecyclerItemClickListener.updatePosition(position);
        ((MyViewHolder) holder).position.setText("" + position);
        ((MyViewHolder) holder).title.setText(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    private class MyViewHolder extends RecyclerView.ViewHolder {
        private OnRecyclerItemClickListener onRecyclerItemClickListener;
        private TextView position;
        private TextView title;

        public MyViewHolder(View itemView, OnRecyclerItemClickListener onRecyclerItemClickListener) {
            super(itemView);

            itemView.setOnClickListener(onRecyclerItemClickListener);
            this.onRecyclerItemClickListener = onRecyclerItemClickListener;
            this.position = (TextView) itemView.findViewById(R.id.position);
            this.title = (TextView) itemView.findViewById(R.id.title);
        }
    }


    private class OnRecyclerItemClickListener implements View.OnClickListener {
        private int position = -1;

        public void updatePosition(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            if(itemClickListener != null) {
                itemClickListener.onItemClicked(v.findViewById(R.id.position), position, dataList.get(position));
            }
        }
    }
}
